import Hamburger from './modules/Hamburger'

try {
	// маленький гамбургер с начинкой из сыра
	const hamburger = new Hamburger(Hamburger.SIZE_SMALL, Hamburger.STUFFING_CHEESE)

	// забыл, а какая там начинка?
	console.log("Stuffing is: %s", hamburger.getStuffing())

	// добавка из майонеза
	hamburger.addTopping(Hamburger.TOPPING_MAYO)

	// спросим сколько там калорий
	console.log("Calories: %f", hamburger.calculateCalories())

	// сколько стоит
	console.log("Price: %f", hamburger.calculatePrice());

	// я тут передумал и решил добавить еще приправу
	hamburger.addTopping(Hamburger.TOPPING_SPICE)

	// Убрать добавку
	hamburger.removeTopping(Hamburger.TOPPING_SPICE)
	console.log("Have %d toppings", hamburger.getToppings().length)

	// Проверить, большой ли гамбургер?
	console.log("Is hamburger large: %s", hamburger.getSize() === Hamburger.SIZE_LARGE)


	console.log(hamburger)


} catch (e) {
	console.log(`${e.name}: ${e.message}`)
}

