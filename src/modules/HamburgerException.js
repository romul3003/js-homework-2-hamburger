class HamburgerException extends Error {
	constructor(message) {
		super()
		this.name = 'HamburgerException'
		this.message = message
	}
}

export default HamburgerException