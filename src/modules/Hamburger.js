import HamburgerException from './HamburgerException'

class Hamburger {
	static SIZE_SMALL = {
		name: 'SIZE_SMALL',
		price: 50,
		calories: 20
	};
	static SIZE_LARGE = {
		name: 'SIZE_LARGE',
		price: 100,
		calories: 40
	};
	static STUFFING_CHEESE = {
		name: 'STUFFING_CHEESE',
		price: 10,
		calories: 20
	};
	static STUFFING_SALAD = {
		name: 'STUFFING_SALAD',
		price: 10,
		calories: 5
	};
	static STUFFING_POTATO = {
		name: 'STUFFING_POTATO',
		price: 15,
		calories: 10
	};
	static TOPPING_MAYO = {
		name: 'TOPPING_MAYO',
		price: 20,
		calories: 5
	};
	static TOPPING_SPICE = {
		name: 'TOPPING_SPICE',
		price: 15,
		calories: 0
	};

	constructor(size, stuffing) {
		const args = Array.from(arguments)
		const reg = [/^SIZE/, /^STUFFING/]

		if (args.length !== 2) {
			throw new HamburgerException('no size given')
		}

		args.forEach((arg, i) => {
			if (!reg[i].test(arg.name)) {
				throw new HamburgerException(`invalid size '${arg.name}'`)
			}
		})

		this._size = size
		this._stuffing = stuffing
		this._toppings = []
	}

	addTopping(topping) {
		this._toppings.forEach(item => {
			if (item === topping) {
				throw new HamburgerException(`duplicate topping ${topping.name}`)
			}
		})

		this._toppings.push(topping)
	}

	removeTopping(topping) {
		this._toppings.forEach(function (item, i, toppings) {
			if (item === topping) {
				toppings.splice(i, 1)
			}
		})
	}

	getToppings() {
		return this._toppings
	}

	getSize() {
		return this._size
	}

	getStuffing() {
		return this._stuffing.name
	}

	calculateCalories() {
		const toppingsCalories = this._toppings.reduce(function (sum, current) {
			return sum + current.calories
		}, 0)

		return this._stuffing.calories + this._size.calories + toppingsCalories
	}

	calculatePrice() {
		const toppingsPrice = this._toppings.reduce(function (sum, current) {
			return sum + current.price
		}, 0)

		return this._stuffing.price + this._size.price + toppingsPrice
	}
}

export default Hamburger